#!/usr/bin/env python3

import fastentrypoints

from setuptools import setup, find_packages

setup(
    name="sa868",
    version="0.1.0",
    author="Chris Fiege",
    author_email="chris@tinyhost.de",
    description="Command line tool to control a SA868 Walkie-Talkie Module",
    packages=['sa868'],
    entry_points={
        'console_scripts': [
            'sa868 = sa868.__init__:main',
        ]
    },
    install_requires=[
        'pyserial',
    ],
)
